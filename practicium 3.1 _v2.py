# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 08:50:38 2018

@author: tretron
"""
import numpy as np
import matplotlib.pyplot as plt
from numpy import matrix

F = 0
h = 1
L = 50
labda_staal = -58
labda_aluminium = -237
teller = 1
X=0

n = int(L/h)

F1 = np.zeros(shape=(n+1,2), dtype= float)
W1 = []
F=0
Km = np.zeros(shape = (n+1, n+1), dtype=float)
Km[np.arange(n+1), np.arange(n+1)] =-2
Km[np.arange(n), np.arange(n)+1] = 1
Km[np.arange(n)+1, np.arange(n)] = 1
print ("bij = ", h)
print (Km)
K1 = matrix(Km.dot((1/(h**2))))
K1 = K1.I
F1 = []
X1 = []
X=0
    
for teller in range (0, n+1):
    if (teller == 0):
        F= ((5*X-0.1*X**2)/labda_aluminium)-(10/(h**2))
    elif (teller == n):
        F= ((5*X-0.1*X**2)/labda_aluminium)-(40/(h**2))
    else:
        F=(5*X-0.1*X**2)/labda_aluminium
        
    F1.append(F)
    X1.append(X)
    X= X+h
F1= matrix(F1)
F1 = F1.T
W1 = K1*F1


F = 0
h = 0.1
L = 50
labda_staal = -58
labda_aluminium = -237
teller = 1
X=0

n = int(L/h)

F2 = np.zeros(shape=(n+1,2), dtype= float)
W2 = []
F=0
Km = np.zeros(shape = (n+1, n+1), dtype=float)
Km[np.arange(n+1), np.arange(n+1)] =-2
Km[np.arange(n), np.arange(n)+1] = 1
Km[np.arange(n)+1, np.arange(n)] = 1
print ("bij = ", h)
print (Km)
K2 = matrix(Km.dot((1/(h**2))))
K2 = K2.I
F2 = []
X2 = []
X=0
    
for teller in range (0, n+1):
    if (teller == 0):
        F= ((5*X-0.1*X**2)/labda_aluminium)-(10/(h**2))
    elif (teller == n):
        F= ((5*X-0.1*X**2)/labda_aluminium)-(40/(h**2))
    else:
        F=(5*X-0.1*X**2)/labda_aluminium
        
    F2.append(F)
    X2.append(X)
    X= X+h
F2= matrix(F2)
F2 = F2.T
W2 = K2*F2

F = 0
h = 0.1
L = 50
labda = -58
teller = 1
X=0

n = int(L/h)

F3 = np.zeros(shape=(n+1,2), dtype= float)
W3 = []
F=0
Km = np.zeros(shape = (n+1, n+1), dtype=float)
Km[np.arange(n+1), np.arange(n+1)] =-2
Km[np.arange(n), np.arange(n)+1] = 1
Km[np.arange(n)+1, np.arange(n)] = 1
print ("bij = ", h)
print (Km)
K3 = matrix(Km.dot((1/(h**2))))
K3 = K3.I
F3 = []
X3 = []
X=0
    
for teller in range (0, n+1):
    if (teller == 0):
        F= ((5*X-0.1*X**2)/labda)-(10/(h**2))
    elif (teller == n):
        F= ((5*X-0.1*X**2)/labda)-(40/(h**2))
    else:
        F=(5*X-0.1*X**2)/labda
        
    F3.append(F)
    X3.append(X)
    X= X+h
F3= matrix(F3)
F3 = F3.T
W3 = K3*F3

W2[0]=10
W2[500]=40
W1[0]=10
W1[50]=40







print('Lagenda: staal = rood, aluminium = blauw')
plt.plot(X1,W1,'r', X2,W2, 'b', X3, W3)
plt.ylabel('Temperatuur in C')
plt.xlabel('Lengte in cm')
plt.show()
