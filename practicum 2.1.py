# -*- coding: utf-8 -*-
"""
Created on Thu Feb  1 16:35:31 2018

@author: tretron
"""


import numpy as np

import matplotlib.pyplot as plt

x_max=1.2
h=0.005

x_start = 1
y_start = 2.3

n=(x_max-x_start)/h


def DVDT(x,y):
    mew= (x**2+y**2)
    return(mew)


def modified_euler(x_start,y_start,n,h):
    y_bar = []
    y = [y_start]
    x = [x_start]
    i = 0
    while i<=n:
        x.append(x[i]+h)
        y_bar.append( y[i] + h*DVDT(x[i], y[i]))
        y.append( y[i]+(h/2)*(DVDT(x[i], y[i])+DVDT(x[i], y_bar[i])))
        i = i+1
    return( x, y )

def find_x (reeks,target):
    reeks_x = reeks[0]
    reeks_y = reeks[1]
    n = len(reeks_x)
    i = 0
    value = 0
    while i < n:
        if (reeks_x[i]<=target+0.005) and (reeks_x[i]>= target-0.005):
            value = reeks_y[i]
        i+=1
    return(value, target)


reeks = modified_euler(x_start, y_start, n, h)

print("waarde bij benadering van F(1.1) = ", find_x(reeks, 1.1))
print("waarde bij benadering van F(1.2) = ", find_x(reeks, 1.2))

plt.plot(reeks[0], reeks[1])
plt.ylabel('Y waarden')
plt.xlabel('X waarden')
plt.show()




    