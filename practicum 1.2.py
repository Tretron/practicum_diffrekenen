# -*- coding: utf-8 -*-
"""
Created on Thu Feb  1 12:56:28 2018

@author: tretron
"""


import numpy as np

import matplotlib.pyplot as plt

nummerriek1  = []
nummerriek2  = []
nummerriek3 = []
nummerriekx1 = []
nummerriekx1 = []
nummerriekx1 = []

y_start = 1.0
x_start = 2.0

H1 = 0.5
H2 = 0.1
H3 = 0.01

n = 10
i = 0


def delta_Y (X_temp,Y_temp):
    temp = 1+(X_temp-Y_temp)**2
    return(temp)

def new_Y (X_new,Y_new1,H_new):
    Y_new = Y_new1+(delta_Y(Y_new1,X_new)*H_new)
    return (Y_new)

def give_nummiriek_calculated(x_start, y_start, H_give , n):
    print("stapgroote = ", H_give)
    list_of_values =[]
    list_of_x = []
    Y =y_start
    X =x_start 
    i=0
    while i<n:
        list_of_values.append(Y)
        list_of_x.append(X)
        X= X+H_give
        Y= new_Y(X,Y,H_give)
        i = i+1
    return (list_of_x,list_of_values)

def give_x_values (x_start, H, n):
    i = 0
    x = []
    while i<n:
        x.append(x_start+(H*i))
        i = i+1
    return (x)

def excact_cal(x_start, H, n):
    exact=[]
    i=0
    x_exact = x_start
    x_exact_list = []
    while i<n:
        y_cal= 1-x_exact
        y_cal= 1/y_cal
        y_cal = x_exact+y_cal
        exact.append(y_cal)
        x_exact_list.append(x_exact)
        x_exact= x_exact+H
        i= i+1
    return [exact, x_exact_list]

def MSE(Yn, Wn):
    i = 0
    n = len(nummerriek1)
    sum_mse = 0
    while i<n:
        sum_mse=sum_mse+(Yn[i]-Wn[i])**2
        i = i+1
    
    return(sum_mse*(1/n))
        
    
nummerriek1 =give_nummiriek_calculated(x_start, y_start, H1, n)
nummerriekx1 = nummerriek1[0]
nummerrieky1 = nummerriek1[1]

nummerriek2 =give_nummiriek_calculated(x_start, y_start, H2, (n*5))
nummerriekx2 = nummerriek2[0]
nummerrieky2 = nummerriek2[1]

nummerriek3 =give_nummiriek_calculated(x_start, y_start, H3, (n*50))
nummerriekx3 = nummerriek3[0]
nummerrieky3 = nummerriek3[1]

exact_list = excact_cal(x_start, H3, n*50)

l1 = excact_cal(x_start, H1, n)
l2 = excact_cal(x_start, H2, (n*5))
l3 = exact_list

plt.plot(nummerriekx1, nummerrieky1,'r' , nummerriekx2, nummerrieky2 ,'b', nummerriekx3, nummerrieky3,'g', exact_list[1], exact_list[0], 'y')
plt.ylabel('Y values')
plt.xlabel('x values')
plt.show()
print('MSE waarde bij H= 0,5 :', MSE(nummerriek1[1], l1[0]))
print('MSE waarde bij H= 0,1 :', MSE(nummerriek2[1], l2[0]))
print('MSE waarde bij H= 0,01 :', MSE(nummerriek3[1],l3[0]))