# -*- coding: utf-8 -*-
"""
Created on Fri Feb  2 09:17:23 2018

@author: tretron
"""


import numpy as np

import matplotlib.pyplot as plt

x_max=10
H1=0.5
H2=0.1
H3=0.01

x_start = 2
y_start = 1


def stap_groote(h, x_start, x_max):
    n=(x_max-x_start)/h
    return(int(n))

def DVDT(x,y):
    mew= 1+((x-y)**2)
    return(mew)

def modified_euler(x_start,y_start,n,h):
    y_bar = []
    y = [y_start]
    x = [x_start]
    i = 0
    while i<=n:
        x.append(x[i]+h)
        y_bar.append( y[i] + h*DVDT(x[i], y[i]))
        y.append( y[i]+(h/2)*(DVDT(x[i], y[i])+DVDT(x[i], y_bar[i])))
        i = i+1
    return( x, y )
        
def MSE(Yn, Wn):
    i = 0
    n = len(Wn)
    sum_mse = 0
    while i<n:
        sum_mse=sum_mse+(Yn[i]-Wn[i])**2
        i = i+1
    
    return(sum_mse*(1/n))
def exacte_oplossing(x,y,n,h):
    i=0
    reeks_y = []
    reeks_x = []
    while i<n:
        y = 1-x
        y = 1/y
        y = x+y
        x = x+h
        i = i+1
        reeks_y.append(y)
        reeks_x.append(x)
    return(reeks_x,reeks_y)

N1 = stap_groote(H1, x_start, x_max)
N2 = stap_groote(H2, x_start, x_max)
N3 = stap_groote(H3, x_start, x_max)
reeks_euler_1=modified_euler(x_start, y_start, N1, H1)
reeks_euler_2=modified_euler(x_start, y_start, N2, H2)
reeks_euler_3=modified_euler(x_start, y_start, N3, H3)
reeks_exact = exacte_oplossing(x_start, y_start, N3, H3)
print(reeks_euler_1[1])
plt.plot(reeks_euler_1[0],reeks_euler_1[1],reeks_euler_2[0],reeks_euler_2[1],reeks_euler_3[0],reeks_euler_3[1],reeks_exact[0],reeks_exact[1])
plt.ylabel('Y waarden')
plt.xlabel('X waarden')
plt.show()
l1= exacte_oplossing(x_start, y_start, N1, H1)
l2= exacte_oplossing(x_start, y_start, N2, H2)
l3 = reeks_exact

print("MSE van ", H1, " Is ", MSE(reeks_euler_1[1], l1[1]))
print("MSE van ", H2, " Is ", MSE(reeks_euler_2[1], l2[1]))
print("MSE van ", H3, " Is ", MSE(reeks_euler_3[1], l3[1]))