
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  2 11:30:11 2018

@author: tretron
"""
import numpy as np
import matplotlib.pyplot as plt
from numpy import matrix

F = 0
h1 = 1
L = 50
labda_staal = -58
labda_aluminium = -237
teller = 1
X=0

def stappen(h,l):
    return(int(l/h))

def nummerieke_benadering(X,teller, labda, n, h):
    F1 = np.zeros(shape=(n+1,2), dtype= float)
    W1 = []
    
    Km = np.zeros(shape = (n+1, n+1), dtype=float)
    Km[np.arange(n+1), np.arange(n+1)] =-2
    Km[np.arange(n), np.arange(n)+1] = 1
    Km[np.arange(n)+1, np.arange(n)] = 1
    K1 = matrix(Km.dot((1/(h**2))))
    K1 = K1.I
    F1 = []
    X1 = []
    
    for teller in range (0, n+1):
        if (teller == 0):
            F= ((5*X-0.1*X**2)/labda)-(10/(h**2))
        elif (teller == n):
            F= ((5*X-0.1*X**2)/labda)-(40/(h**2))
        else:
            F=(5*X-0.1*X**2)/labda
        
        F1.append(F)
        X1.append(X)
        X= X+h
    F1= matrix(F1)
    F1 = F1.T
    W1 = K1*F1
    return(W1,X1)
staal_1 = nummerieke_benadering(X, teller, labda_staal, stappen(h1,L), h1)
aluminium = nummerieke_benadering(X, teller, labda_aluminium, stappen(h1,L), h1)

print('Lagenda: staal = rood, aluminium = blauw')
plt.plot(staal_1[1], staal_1[0],'r' ,aluminium[1], aluminium[0], 'b')
plt.ylabel('Temperatuur in C')
plt.xlabel('Lengte in cm')
plt.show()

