# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 15:43:31 2018

@author: tretron
"""

import numpy as np
import matplotlib.pyplot as plt

Massa = 320.0
C = 10.0
Fm = 150.0
h = 28.8

"formula is dv/dt = Fmm - (C*v)/m | waarbij Fmm = Fm/m"
fmm = (Fm/Massa)
"aantal stappen"
n = 10
i = 0

toud = 0.0
voud = 0.0
euler_benadering = []

def euler(h,n):
    t=0.0
    v=0.0
    i=0
    benadering=[]
    tijd=[]
    while i<n:
        benadering.append(v)
        tijd.append(t)
        DVDT=(150-(10*v))/320
        t=t+h
        v=v+DVDT*h
        i = i+1
    return(benadering,tijd)

euler_benadering = euler(h,n)

def formule(h, n):
    i = 0
    t= 0.0
    v= 0.0
    v_reeks = []
    t_reeks = []
    while i < n:
        v_reeks.append(v)
        t_reeks.append(t)
        t = t+h
        v = -15*(np.exp(-t/32))+15
        
        
        i= i+1
    return(v_reeks, t_reeks)

exacte_snelheid = formule(h,n)

def calc_90(reeks):
    reeks_x = reeks[1]
    reeks_y = reeks[0]
    n = len(reeks)
    i=0
    while i<n:
        if(reeks_y[i]<= 13.5+5) and (reeks_y[i]>=13,5-5):
            value = reeks_x[i]
            y = reeks_y[i]
        i+=1
    return(value,y)

snelheid90 = []
i = 0
while i<n:
    snelheid90.append(13.5) #snelheid heeft een assymtooth met 15 m/s, 0,90*15 = 13,5
    i= i+1



print("Cordinaten snelheid is bij benadering 13,5 = ", calc_90(euler_benadering))
plt.plot(euler_benadering[1],euler_benadering[0],'r' , exacte_snelheid[1], exacte_snelheid[0],'b',exacte_snelheid[1],snelheid90)
plt.ylabel('Snelheid in M/s')
plt.xlabel('tijd in seconden')
plt.show()




    
    
    
    
    
    
    