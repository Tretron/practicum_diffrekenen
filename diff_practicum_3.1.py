# -*- coding: utf-8 -*-
"""
Created on Fri Feb  5 11:40:11 2018

@author: Tretron
"""
import numpy as np
import matplotlib.pyplot as plt

### a1)-----------------------------------------------------------------------
xstep = 0.1
L = 50
lamb = 237
xs = np.arange(0,50,xstep)
wn1 = 40
w0 = 10

A = np.zeros((len(xs), len(xs)))
f=[]
for i,x in enumerate(xs[:-1]):
    # A = M + K
    A[i,i] =  -2/(xstep**2)
    A[i,i+1] = 1/(xstep**2)
    A[i+1,i] = 1/(xstep**2)
    f.append((5*x-0.1*x**2)/-lamb) 

A[i+1,i+1] = -2/(xstep**2)
f.append((5*xs[-1]-0.1*xs[-1]**2)/-lamb - wn1/(xstep**2))
f[0] = (5*xs[0]-0.1*xs[0]**2)/-lamb - w0/(xstep**2)

invA = np.linalg.inv(A)
w = np.dot(f,invA)

plt.plot(xs,w,label = 'aluminum step = 0.1')

### a2)-----------------------------------------------------------------------
xstep = 1
L = 50
lamb = 237
xs = np.arange(0,51,xstep)
wn1 = 40
w0 = 10

A = np.zeros((len(xs), len(xs)))
f=[]
for i,x in enumerate(xs[:-1]):
    # A = M + K
    A[i,i] =  -2/(xstep**2)
    A[i,i+1] = 1/(xstep**2)
    A[i+1,i] = 1/(xstep**2)
    f.append((5*x-0.1*x**2)/-lamb) 

A[i+1,i+1] = -2/(xstep**2)
f.append((5*xs[-1]-0.1*xs[-1]**2)/-lamb - (wn1-4)/(xstep**2))
f[0] = (5*xs[0]-0.1*xs[0]**2)/-lamb - (w0-4)/(xstep**2)

invA = np.linalg.inv(A)
w = np.dot(f,invA)

plt.plot(xs,w,label = 'aluminum step = 1')



### b)------------------------------------------------------------------------
xstep = 0.1
L = 50
lamb = 58
xs = np.arange(0,50,xstep)
wn1 = 40
w0 = 10

A = np.zeros((len(xs), len(xs)))
f=[]
for i,x in enumerate(xs[:-1]):
    # A = M + K
    A[i,i] =  -2/(xstep**2)
    A[i,i+1] = 1/(xstep**2)
    A[i+1,i] = 1/(xstep**2)
    f.append((5*x-0.1*x**2)/-lamb) 

A[i+1,i+1] = -2/(xstep**2)
f.append((5*xs[-1]-0.1*xs[-1]**2)/-lamb - wn1/(xstep**2))
f[0] = (5*xs[0]-0.1*xs[0]**2)/-lamb - w0/(xstep**2)

invA = np.linalg.inv(A)
w = np.dot(f,invA)

plt.plot(xs,w,label = 'steel step = 0.1')
plt.legend()
plt.title('Temperature verdeling')
plt.xlabel('x')
plt.ylabel('T')
plt.show()